package com.sf.example.entity;

/**
 * Account base class added in Kotlin code.
 */
class Account: BaseEntityObject() {
    lateinit var masterRecordId:String
    lateinit var type:String
    lateinit var parentId:String
    lateinit var billingStreet:String
    lateinit var billingCity:String
    lateinit var billingState:String
    lateinit var billingPostalCode:String
    lateinit var billingCountry:String
    lateinit var billingLatitude:String
    lateinit var billingLongitude:String
    lateinit var billingGeocodeAccuracy:String
    lateinit var billingAddress:String
    lateinit var shippingStreet:String
    lateinit var shippingCity:String
    lateinit var shippingState:String
    lateinit var shippingPostalCode:String
    lateinit var shippingCountry:String
    lateinit var shippingLatitude:String
    lateinit var shippingLongitude:String
    lateinit var shippingGeocodeAccuracy:String
    lateinit var shippingAddress:String
    lateinit var phone:String
    lateinit var fax:String
    lateinit var accountNumber:String
    lateinit var website:String
    lateinit var photoUrl:String
    lateinit var sic:String
    lateinit var industry:String
    lateinit var annualRevenue:String
    lateinit var numberOfEmployees:String
    lateinit var ownership:String
    lateinit var tickerSymbol:String
    lateinit var description:String
    lateinit var rating:String
    lateinit var site:String
    lateinit var ownerId:String
    lateinit var jigsaw:String
    lateinit var jigsawCompanyId:String
    lateinit var cleanStatus:String
    lateinit var accountSource:String
    lateinit var dunsNumber:String
    lateinit var tradestyle:String
    lateinit var naicsCode:String
    lateinit var naicsDesc:String
    lateinit var yearStarted:String
    lateinit var sicDesc:String
    lateinit var dandbCompanyId:String
    lateinit var operatingHoursId:String
    lateinit var customerPriority__c:String
    lateinit var slA__c:String
    lateinit var active__c:String
    lateinit var numberofLocations__c:String
    lateinit var npsellOpportunity__c:String
    lateinit var slaSerialNumber__c:String
    lateinit var slaExpirationDate__c:String
}