package com.sf.example.entity;

open class BaseEntityObject {
    lateinit var id:String
    open lateinit var name:String
    lateinit var createdDate:String
    lateinit var createdById:String
    lateinit var lastModifiedDate:String
    lateinit var lastModifiedById:String
    lateinit var systemModstamp:String
    lateinit var lastActivityDate:String
    lateinit var lastViewedDate:String
    lateinit var lastReferencedDate:String
}