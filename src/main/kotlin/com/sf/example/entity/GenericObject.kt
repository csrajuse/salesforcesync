package com.sf.example.entity;

/**
 * For having any object be stored in the database and sync, we will be having a generic object that can store using meta-data.
 */
class  GenericObject: BaseEntityObject() {
    val dataMap = mutableMapOf<String,String>()
    fun setData(key:String, value:String) {
        dataMap.put(key,value)
    }
    fun getData(key:String): String? {
        return dataMap.get(key)
    }
}