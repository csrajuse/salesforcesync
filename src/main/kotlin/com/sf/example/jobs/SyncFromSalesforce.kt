package com.sf.example.jobs

import com.sf.example.repository.SalesforceSyncfromRepository
import com.sf.example.service.SyncService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.*

@Component
class SyncFromSalesforce{

    @Autowired
    private lateinit var syncService: SyncService

    @Autowired
    private lateinit var salesforceSyncfromRepository: SalesforceSyncfromRepository

    @Value("\${objects_to_sync}")
    private lateinit var objectsToSync:String

    @Scheduled(fixedRate=10*60*1000)
    fun syncObjects(){
        try {
            val objectNames =objectsToSync.split(",")
            for(objectName in objectNames){
                logger.info("Syncing objects from salesforce:$objectName" )
                syncService.syncFromSalesForce(objectName, salesforceSyncfromRepository.fetchLatestSyncTime(objectName))
            }
        }catch(ex:Exception){
            logger.error(ex.message,ex)
        }
    }

    companion object{
        private val logger = LoggerFactory.getLogger("SyncFromSalesforce");
    }
}