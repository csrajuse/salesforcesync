package com.sf.example.jobs

import com.sf.example.repository.SalesforceSyncfromRepository
import com.sf.example.service.SyncService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class SyncToSalesForce{

    @Autowired
    private lateinit var syncService: SyncService

    @Autowired
    private lateinit var salesforceSyncfromRepository: SalesforceSyncfromRepository

    @Value("\${objects_to_sync}")
    private lateinit var objectsToSync :String

    @Scheduled(fixedRate = 10 * 60 * 1000)
    fun syncToSalesforce(){
        val objectNames = objectsToSync.split(",")

        for(objectName in objectNames){
            logger.info("Syncing objects to salesforce:$objectName");
            syncService.syncFromSalesForce(objectName, salesforceSyncfromRepository.fetchLatestSyncTime(objectName))
        }
    }

    companion object{
        private val logger = LoggerFactory.getLogger("SyncToSalesForce")
    }
}