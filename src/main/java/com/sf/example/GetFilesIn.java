package com.sf.example;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetFilesIn {

    SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy:hh:mm:ss Z");

    /**
     * Method for parsing each file
     * @param file
     * @return
     */
    public String parseEachFile(File file) throws ParseException{

        //BufferedReader reader = null;
        Map<String,MonthAgreResults> perMonthMap = new HashMap<>();
        try{
            Scanner scanner = new Scanner(file);

            while(scanner.hasNext()){
                String readLine = scanner.nextLine();
                Result result = processString(readLine);
                if(result!=null) {
                    if(perMonthMap.get(result.yearMonth)==null){
                        perMonthMap.put(result.yearMonth,new MonthAgreResults());
                    }
                    perMonthMap.get(result.yearMonth).incrementVisitor(result.ipAddress);
                    perMonthMap.get(result.yearMonth).incrementBytes(Integer.parseInt(result.totalBytes));
                    perMonthMap.get(result.yearMonth).incrementCodes(result.code);
                }
            }

            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(perMonthMap);

        }catch(IOException ioe){
            //Prefer using a logger here.
            throw new RuntimeException(ioe);
        }

    }

    public Result processString(String record) throws ParseException {
        // Creating a regular expression for the records
        final String regex = "^(\\S+) (\\S+) (\\S+) " +
                "\\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(\\S+)" +
                " (\\S+)\\s*(\\S+)?\\s*\" (\\d{3}) (\\S+)";

        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(record);

        Result result = null;

        while (matcher.find()) {
            String ip = matcher.group(1);
            String dateStr = matcher.group(4);
            String response = matcher.group(8);
            String bytes = matcher.group(9);
            Date date = formatter.parse(dateStr);
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTime(date);
            String yearMonth = String.valueOf(cal.get(Calendar.YEAR))+String.valueOf(cal.get(Calendar.MONTH));
            int responseInt = Integer.parseInt(response);
            result = new Result(ip,bytes,responseInt,yearMonth);
        }
        return result;
    }

    private static class Result{
        public String ipAddress;
        public String totalBytes;
        public int code;
        public String yearMonth;


        Result(String ipAddress,String totalBytes,int code,String yearMonth){
            this.ipAddress = ipAddress;
            this.totalBytes = totalBytes;
            this.code = code;
            this.yearMonth = yearMonth;
        }
    }

    private static class MonthAgreResults{
        public int visitors = 0;
        public int total_bytes = 0;
        public Map<Integer,Integer> codes = new HashMap<>();
        //This is supposed to be unique
        public Set<String> visitorsSet = new HashSet<>();

         void incrementVisitor(String ip){
             visitorsSet.add(ip);
             visitors = visitorsSet.size();
         }

         void incrementBytes(int bytes){
             total_bytes = total_bytes+bytes;
         }

         void incrementCodes(int code){
             Integer count = codes.get(code);
             if(count == null){
                 codes.put(code,1);
             }
             codes.put(code,codes.get(code)+1);
         }

         String getVisitors(){
             return String.valueOf(visitors);
         }

         String getTotal_bytes(){
             return String.valueOf(total_bytes);
         }

         Map<Integer,Integer> getCodes(){
            return codes;
         }
    }

    public static void main(String arg[]) throws Exception{
        GetFilesIn in = new GetFilesIn();
        try {
            // Create temp file.
            File temp = File.createTempFile("temp", ".tmp");
            // Write to temp file
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            out.write("192.168.2.5 - - [01/May/2017:06:22:10 +0000] \"GET / HTTP/1.1\" 200 677 \"-\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36\n");
            out.write("192.168.2.7 - - [01/May/2017:06:29:13 +0000] \"GET /icons/folder.gif HTTP/1.0\" 200 472 \"http://cdn.dsj.io/\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36\"\n");
            out.write("192.168.2.7 - - [19/May/2017:21:46:02 +0000] \"GET /js/jquery.min.js HTTP/1.0\" 500 33686 \"http://jones6u.com/\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36\"\n");
            out.write("192.168.2.5 - - [27/Jul/2017:14:51:33 -0700] \"GET /img/toggle.jpg HTTP/1.0\" 404 7407 \"-\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.109 Safari/537.36\"\n");
            out.write("172.19.0.1 - - [13/Oct/2020:08:25:55 -0700] \"GET /js/run_prettify.js?lang=go&skin=sunburst HTTP/1.0\" 200 8312 \"http://docs.dsj.io/go/20170721_golang.html\" \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:81.0) Gecko/20100101 Firefox/81.0\"\n");
            out.close();

            System.out.println(in.parseEachFile(temp));

        } catch (IOException e) {
        }
    }

    /*
    # 192.168.2.5 - - [01/May/2017:06:22:10 +0000] "GET / HTTP/1.1" 200 677 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"
    # 192.168.2.7 - - [01/May/2017:06:29:13 +0000] "GET /icons/folder.gif HTTP/1.0" 200 472 "http://cdn.dsj.io/" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"
    # 192.168.2.7 - - [19/May/2017:21:46:02 +0000] "GET /js/jquery.min.js HTTP/1.0" 500 33686 "http://jones6u.com/" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
    # 192.168.2.5 - - [27/Jul/2017:14:51:33 -0700] "GET /img/toggle.jpg HTTP/1.0" 404 7407 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.109 Safari/537.36"
    # 172.19.0.1 - - [13/Oct/2020:08:25:55 -0700] "GET /js/run_prettify.js?lang=go&skin=sunburst HTTP/1.0" 200 8312 "http://docs.dsj.io/go/20170721_golang.html" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:81.0) Gecko/20100101 Firefox/81.0"
     */

}
