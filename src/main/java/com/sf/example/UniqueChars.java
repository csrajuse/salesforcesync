package com.sf.example;

import java.util.ArrayList;
import java.util.List;

public class UniqueChars {

    int uniqueChars(String s) {
        return isPal(s);
    }

    int isPal(String s){
        char[] chars = s.toCharArray();
        int index =0;
        int length = 0;
        String currentString = "";
        while(index < s.length()){
            List<Character> currentList =  new ArrayList<>();
            for(int i=index;i<s.length();i++){
                if(currentList.contains(chars[i])){
                    break;
                }
                else{
                    currentList.add(chars[i]);
                }
            }
            if(currentList.size()>length){
                length = currentList.size();
            }
            index++;
        }
        return length;
    }

    public static void main(String arg[]){
        UniqueChars chars = new UniqueChars();
        System.out.println(""+chars.uniqueChars("ancdefghila"));
    }
}
